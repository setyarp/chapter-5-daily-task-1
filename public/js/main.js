// Add Data Product in dashboard admin
async function addDataProduct() {
    const data = {
      judul: document.getElementById("addTitleBook").value,
      penulis: document.getElementById("addWriter").value,
      tahun_terbit: document.getElementById("addYearPublish").value,
      user_id: document.getElementById("addUserId").value,
  
    };
    if ( data.judul == "" ) {
      alert("Data Tidak Boleh Kosong!");
    }
    else if ( data.penulis == "") {
      alert("Data Tidak Boleh Kosong!");
    }
    else if ( data.tahun_terbit == "") {
      alert("Data Tidak Boleh Kosong!");
    }
    else if ( data.user_id == "") {
      alert("Data Tidak Boleh Kosong!");
    }
     else {
      const restAPI = await axios.post("/api/books", data);
      if (restAPI.data.status) {
        alert("Berhasil Tambah Data");
      } else {
        alert("500 Server Error [Tidak Dapat Menambahkan data]");
      }
    }
  }
  