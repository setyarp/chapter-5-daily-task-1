exports.up = (pgm) => {
    pgm.createTable('users', {
      id: 'id',
      name: { type: 'varchar(1000)', notNull: true },
      createdAt: {
        type: 'timestamp',
        notNull: true,
        default: pgm.func('current_timestamp'),
      },
    })
    pgm.createTable('book', {
      id: 'id',
      judul: { type: 'varchar(1000)', notNull: true },
      pengarang: { type: 'varchar(1000)', notNull: true },
      tahun: { type: 'varchar(1000)', notNull: true },
      iduser: {
        type: 'integer',
        notNull: true,
        references: '"users"',
        onDelete: 'cascade',
      },
      createdAt: {
        type: 'timestamp',
        notNull: true,
        default: pgm.func('current_timestamp'),
      },
    })
    pgm.createIndex('books', 'user_id')
  }