const express = require("express");
const bodyParser = require("body-parser")
const path = require("path");
const PORT = process.env.port || 8000;
const app = express();

const router = require("express").Router();

// our own module
const routes = require("./routes")

const bookController = require('./controllers/bookcontroller')

const {pool} = require("./config/database")

// Assign path directory public and controller
const PUBLIC_DIRECTORY = path.join(__dirname, "public");
const CONTROLLERS_DIRECTORY = path.join(__dirname, "controllers");

// Set format request
app.use(express.urlencoded({ extended: true }));

// static path diectory
app.use(express.static(PUBLIC_DIRECTORY));
app.use(express.static(CONTROLLERS_DIRECTORY));

// Use ejs as view engine
app.set("view engine", "ejs");

// Set JSON
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Routes
app.use(routes)

// app.get("/", (req, res) => {
//   res.render("index", {
//     title: "Halaman Depan",
//     name: req.query.name || " ",
//     data: data,
//   });
// });



// // read books
// app.get('/books', (req, res) =>{
//   pool.query(`Select * from books`, (err, result) =>{
//     if(!err){
//       res
//       .send(result.rows)
//     }
//   })
// })

// // create books
// app.post('/books', (req, res) => {
//   const {judul,penulis,tahun_terbit,user_id} = req.body

//   pool.query((`insert into books(judul,penulis,tahun_terbit,user_id) values('${judul}', '${penulis}', '${tahun_terbit}')`), (err, result)=>{
//     if(!err){
//       res.send('Tambah data berhasil')
//     }else{
//       res.send(err.message)
//     }
//   })
// })

// // update books
// app.put('/books/:id', (req, res) => {
//   const {judul,pengarang,tahun} = req.body

//   pool.query((`update books set judul = '${judul}', pengarang = '${pengarang}', tahun = '${tahun}' where id = '${req.params.id}'`), (err, result)=>{
//     if(!err){
//       res.send('Data berhasil di update')
//     }else{
//       res.send(err.message)
//     }
//   })
// })

// // delete books
// app.delete('/books/:id', (req, res) => {
//   pool.query((`delete from books where id = ${req.params.id}`), (err,result) => {
//     if(!err){
//       res.send('Data berhasil dihapus')
//     }else{
//       res.send(err.message)
//     }
//   })
// })

app.use("/", (req, res) => {
  res.status(404);
  res.render("404");
});

// Cek koneksi ke database
pool.connect(err => {
  if(err){
    console.log(err.message)
  }else{
    console.log("Berhasil terkoneksi ke database crud")
  }
})

app.listen(PORT, () => {
  console.log(`Server berjalan di http://localhost:${PORT}`);
});

