const {pool} = require('../../config/database')
const filter = require('../../public/js/filter');


// // GET /books
//  function getBooks(req, response) {
//   pool.query('SELECT * FROM books', (error, results) => {
//     if (error) throw error;

//     if (!error) {    
//       response.render('table', {results: results.rows})
//     }
//   })
//  }



 // GET /userid
 function getUserId(req, response) {
  pool.query('SELECT * FROM users inner join books on users.id=books.user_id', (error, results) => {
    if (error) throw error;

    if (!error) {
      const search = req.query.user_id;
      let dataSearch = filter(results.rows, search);    
      response.render('index', {results: dataSearch})
    }
  })
 }


 // GET /books/create
function createBook(req, res) {
  res.render("create");
}

// function viewBook(req, res) {
//   res.render("view");
// }

function viewBook(req, response) {
  const id = req.params.id

  pool.query('SELECT * FROM books WHERE id = $1', [id], (error, results) => {
    if (error) throw error;

    if (!error) {
      response.render('view', {results: results})
    }
  })
 }

 function editBookPage(req, res) {
  const id = req.params.id

  pool.query('SELECT * FROM books WHERE id = $1', [id], (error, results) => {
    if (error) throw error;

    if (!error) {
      res.render('edit', {results: results.rows})
    }
  })
}

const editBook = (request, response) => {
  const id = request.params.id
  const {judul, penulis, tahun_terbit, user_id} = request.body

  pool.query(
    'UPDATE books SET judul = $1, penulis = $2, tahun_terbit = $3, user_id = $4 WHERE id = $5',
    [judul, penulis, tahun_terbit, user_id, id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.redirect("/");
    }
  )
}

const hapusBook = (req, res) => {
  const id = req.params.id
  pool.query("DELETE FROM books WHERE id = $1", [id], (err, results) => {
    res.redirect("/");
  });
};


module.exports = {
  // getBooks,
  createBook,
  getUserId,
  viewBook,
  editBookPage,
  editBook,
  hapusBook
}

